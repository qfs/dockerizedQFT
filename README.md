# The QF-Test docker container
------
------

# Prerequisites

## Note of caution

> Using the default configuration, incoming/outgoing are not secured, but rely on plain http. If this behaviour would imply security concerns in your setup, you should think of forwarding all traffic  via **reverse proxy** and disable all direct port forwardings in the [override file](#overrides). A suitable docker container can e.g. be found here: [https://github.com/jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy)

## Checkout the sources
```
git clone ...
cd dockerizedQFT
```

## Populate the [secrets folder](./secrets)
- Change the passwords in the [jenkins-admin_pw.txt](./secrets/jenkins-admin_pw.txt)  and [vnc_pw.txt](./secrets/vnc_pw.txt)
- For jenkins node authorization, a random password might be used:

```
cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 24 > secrets/jenkins-swarm_pw.txt

```
- Copy your license into the secrets folder and name it [license](./secrets/license)

[Docker secrets](https://docs.docker.com/engine/swarm/secrets/) are immutable. If you change on of the secrets mentioned above, a restart of the corresponding services is required.

## Build the containers
```
docker-compose build
```

# Deployment

## Just run it...

```
docker-compose up
```
## ...or with multiple (N) QF-Test working nodes
```
docker compose up --scale qft-node=<N>
```

Now all containers should be up and running, You can verify that via the `docker ps` command, which should list all active containers ([see here](#application-composition)) together with their **CONTAINER ID**s and forwarded **PORTS**.

## Fundamental docker concepts

> This section covers fundamental docker concepts that might become handy if you use the containers. In case of a first tryout it can be skipped and you can directly proceed with the [next section](#qf-test-configuration).

###  Application  composition
The [docker-compose.yaml](docker-compose.yaml) file describes two type of containers that are going to be exectued: A **(jenkins-)Master** and **QF-Test (working) nodes**. The former controls test execution, whereas actual testing is done on the working nodes.

Both image types declare a persistent volume, which is simultaneously the **home directory** of the **main user** of that container. The following table summarizes relevant image properties:

| Image | Persistent volume / "home" directory | Exposed ports | Environment variables |
|-------|----------------------|--------------|-----------------------|
| qfs/jenkins_master | /var/jenkins_home | 8080, (6080) | START_NOVNC |
| qfs/qft-node | /home/seluser | (5900) | VNC_NO_PASSWORD |


#### Overrides

Obviously, you might want to adapt the original compose file to your own needs. In addition, such enhancements to the original compose file can be put in a [special override file](./docker-compose.override.yaml), which is automatically considered when setting up the application. Port forwarding is defined here (so that it is automatically switched off when the override file is absent). Furthermore,  other -- potentially useful -- configuration tweeks are suggested here (just comment them in, if needed).

### Data sharing
In principle, there are two ways of changing files within the docker containers (presumably located on one the persistent volume):

- Directly bind a specific directory to a location on the host machine. **Attention**: This approach requires consistent `uid/gid` settings between the host and the client side. The qf-test node itself uses standard ids: `uid=1000(seluser) gid=1000(seluser)` The [override file](./docker-compose.override.yaml) illustrates how to perform a bind mount in case of the `qft-workspace` (cf. the [configuration section ](#qf-test-configuration))

- Similar to `scp`, you can use `docker cp` to transfer files to a running container (and accessible volumes). E.g. to copy a test suite into the [default suite directory](#qf-test-configuration) of the docker image run the following (_The container must be running, look up its_ **CONTAINER_ID** _via_ `docker ps`):
```
docker cp <path_to_testsuite.qft> <CONTAINER_ID>:/home/seluser/qft-workspace/suites
```

# QF-Test configuration

QF-Test itself resides on the working nodes. Configuration files and places are located here:

| directory | absolute path |
|-|-|
| userconfig | /home/seluser/.qftest |
| systemconfig | /home/seluser/qft-workspace |
| default suite directory | /home/seluser/qft-workspace/suites |

If you want the `qft-workspace` to be directly accessible from the docker host, consider a [bind mount](#data-sharing) of the entire directory.


# Usage

## Together with the Jenkins-Master

### Test execution

In addition to the working nodes, this project ships a jenkins service, with the **QF-Test jenkins plugin** already installed. It can be used to control job execution on the different nodes. Its http port `:8080` is mapped to the host machine (e.g. [http://localhost:8080](http://localhost:8080)); you can login with user: "admin", password: "_as set in secrets file_".

A _sample build_ is already set up. Per default, the jenkins master will trigger all the suites, which are located under `<qft_node_home>/qft-workspace/suites/` and one of the registered working nodes will execute the test. On its welcome page, jenkins will report on the status of all registered working nodes in the **Build executors status** pane. A **jenkins node name** follows the schema _QFTnode-X_ with _X_ being a randomly generated suffix.

### VNC access
If you want to directly observe test execution or [interact manually](#manual-interactive-mode) with the **QF-Test** instance you can access them by means of the **vnc protocol**. A **novnc** instance has been installed on the jenkins master. It offers a vnc viewer solution based on web-technology and proxys your requests form the master to the correct node. Unless you set the environment variable `START_NOVNC=no` within the [overrride file](./docker-compose.override.yaml), this service is automatically started on the jenkins master node. You can reach all registered QF-Test nodes under to the following URL in your web-browser (Please replace `<docker-host>` and `<node_name>` with their corresponding address):
    `http://<docker-host>:6080/vnc.html?host=<docker-host>&port=6080&path=%3Ftoken%3D<node_name>%3A5900`
    The password is taken from the provided secrets. Setting the environment variable `VNC_NO_PASSWORD=1` (cf. the [overrride file](./docker-compose.override.yaml) file) grants access without password query.

## Manual/Interactive mode
In grapchical mode, the QF-Test node presents the fluxbox desktop. Within a VNC session you can start applications like **`firefox`, `chrome` and `qftest`** manually.
>**The context menu is accessible via right click on the empty desktop!**

You can either connect to the vnc session via the [novnc service of the jenkins master](VNC-access) or directly forward the VNC port `:5900` of the working nodes to the host side. For the latter, please uncomment the relevant line in the [override file](./docker-compose.override.yaml). If you use more than one working node in parallel, the port number on the host side has to be unique, i.e. the actual number must not be specified. In this case, docker will choose a free port for you, the actual value can be retrieved from the `docker ps` command.


## Maintenance
### Root access
Due to the underlying ubuntu image, the root account is disabled. If you need root access within the QF-Test container, use **sudo**:  
```
sudo [your command] #no admin password needed
```
An interactive root shell can therefore be achieved as such: `sudo ${SHELL}`

### The QF-Test executable file

In order to make the [default system configuration](#qf-test-configuration) work, the `qftest` executable has been wrapped by the file [/usr/local/bin/qftest](./docker-QFTnode/qftest), given priority to by the `$PATH` variable. It calls the correct executable and provides default values for **system-** and **userdir** and makes **your license** known to **QF-Test**


------
------

# Development

### Submodules
For reference and further development, this projects links in several submodules used to build this application:
- [Selenium](./docker-selenium): The qftest docker image is based on a selenium docker (chrome-debug node standalone) image.
- [Jenkins](./docker-jenkins): The jenkins-master node (equipped with the **QF-Test plugin** and a **novnc** instance**


### Drop-ins

Similar to [overrides](#Overrides), `docker-compose` allows the definition of **drop-in files**. If you want to consider these, you have to combine one or more of these fragments with the original compose file by means of the `-f` option when issuing the `docker compose up` command. E.g., if you want to activate remote debugging of the jenkins instance on port `:5005`:
```
docker-compose up -f docker-compose -f ./compose-dropins/jenkinsDebug.yaml -f [./docker-compose.override.yaml] [[ -f ... ]]
```

In this case the override file has to be specified explicit (if requested). Consult the [official documentation](https://docs.docker.com/compose/extends/) if you want to learn more about this concept.

------
------

# Troubleshooting

 - While executing a docker file and although the "-y" parameter is given, apt-get may sometimes print questions that require special handling, e.g.

```
Configuration file '/etc/apt/apt.conf.d/01autoremove'
 ==> File on system created by you or by a script.
 ==> File also in package provided by package maintainer.
   What would you like to do about it ?  Your options are:
    Y or I  : install the package maintainer's version
    N or O  : keep your currently-installed version
      D     : show the differences between the versions
      Z     : start a shell to examine the situation
 The default action is to keep your current version.
*** 01autoremove (Y/I/N/O/D/Z) [default=N] ? dpkg: error processing package apt (--configure):
 end of file on stdin at conffile prompt
Errors were encountered while processing:
 apt
E: Sub-process /usr/bin/dpkg returned an error code (1)
```

in this case adding ```echo "<selection>"``` to the corresponding ```apt-get``` command, e.g. ```echo "N" | apt-get install -y libgtk2.0-0 jq curl``` will help. For some - unfortunately
not all - questions preappending ```DEBIAN_FRONTEND=noninteractive``` may work too.

 -  On windows computers a "b'Drive has not been shared'" error may pop up. In
this case the following steps may help:

   1. On Command Line: "set COMPOSE_CONVERT_WINDOWS_PATHS=1"
   2. Restart Docker for Windows
   3. Go to Docker for Windows settings > Shared Drives > Reset credentials > select drive > Apply
   4. Reopen Command Line
   5. Kill the Containers
   6. Rerun the Containers
