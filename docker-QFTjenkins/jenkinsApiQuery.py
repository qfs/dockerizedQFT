import urllib
import xml.etree.ElementTree as ET
import re


class JenkinsNodeToken(object):
    """
    Querys the jenkins api in order to find the ip of a node
    that has been added dynamically (i. e. via the swarm plugin)
    """

    api_call = '/computer/api/xml?depth=0'
    xpath = "computer[@_class='hudson.slaves.SlaveComputer'][displayName='{tok}']/description"
    ip_reg = re.compile(re.compile('^Swarm slave from ([\d\.]+)'))

    def __init__(self, src):
        """ src: url of the jenkins instance to query"""
        self.jenkins_server = src

    def lookup(self, token):
        """ The lookup token for a specific jenkins node should be of the form: <displayName>:<port>"""

        tok, port = token.split(':')

        url = urllib.basejoin(self.jenkins_server, self.api_call)
        ret = ET.parse(urllib.urlopen(url))
        desc = ret.findall(self.xpath.format(tok=tok))

        if len(desc) == 0:
            return None
        elif len(desc) == 1:
            desc = desc[0]
            ip = self.ip_reg.match(desc.text).group(1)
            return ip, port
        else:
            raise ValueError("more than 1 node matched")

